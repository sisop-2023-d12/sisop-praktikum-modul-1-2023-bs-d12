# Soal Shift 1 2023

## 0. Peraturan

1. Waktu pengerjaan dimulai Senin (27/2) pukul 10.00 WIB hingga Jumat (3/3) pukul 22.00 WIB.
2. Praktikan diharapkan membuat laporan penjelasan dan penyelesaian soal dalam bentuk Readme(gitlab).
3. Format nama repository gitlab “sisop-praktikum-modul-[Nomor Modul]-2023-[Kode Dosen Kelas]-[Nama Kelompok]” (contoh:sisop-praktikum-modul-1-2023-WS-A11).
4. Struktur repository seperti berikut:

       - readme.md
       - soal1:
         - university_survey.sh
       - soal2:
       	- kobeni_liburan.sh
       - soal3:
         - louis.sh
       	- retep.sh
       - soal4:
       	- log_encrypt.sh
       	- log_decrypt.sh

   Jika **melanggar struktur repo** akan **dianggap sama dengan curang** dan menerima konsekuensi **sama** dengan **melakukan kecurangan** (poin 10).
5. Setelah pengerjaan selesai, semua script bash, awk, dan file yang berisi cron job ditaruh di gitlab masing - masing kelompok, dan link gitlab diletakkan pada form yang disediakan.
6. Commit terakhir maksimal 10 menit setelah waktu pengerjaan berakhir. Jika melewati maka akan dinilai berdasarkan commit terakhir.
7. Jika tidak ada pengumuman perubahan soal oleh asisten, maka soal dianggap dapat diselesaikan.
8. Jika ditemukan soal yang tidak dapat diselesaikan, harap menuliskannya pada Readme beserta permasalahan yang ditemukan.
9. Praktikan tidak diperbolehkan menanyakan jawaban dari soal yang diberikan kepada asisten maupun praktikan dari kelompok lainnya.
10. Jika ditemukan **indikasi kecurangan** dalam bentuk apapun di pengerjaan soal shift, maka **nilai dianggap 0**.
11. Pengerjaan soal shift sesuai dengan modul yang telah diajarkan.
12. Zip dari repository dikirim ke email asisten penguji dengan subjek yang sama dengan nama judul repository, dikirim sebelum deadline dari soal shift

## 1. Bocchi's University Survey

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  :

   - Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
   - Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a.
   - Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
   - Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

## 2. Kobeni's Copium

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

   - Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

     - File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
     - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)

   - Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip `devil_NOMORZIP` dengan `NOMORZIP` adalah urutan folder saat dibuat (`devil_1`, `devil_2`, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

## 3. User System

Peter Griffin hendak membuat suatu sistem register pada script `louis.sh` dari setiap user yang berhasil didaftarkan di dalam file `/users/users.txt`. Peter Griffin juga membuat sistem login yang dibuat di script `retep.sh`

   - Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
     - Mengikuti konvensi penamaan `snake_case`
     - Minimal 8 karakter
     - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
     - Alphanumeric
     - Tidak boleh sama dengan username
     - Tidak boleh menggunakan kata chicken atau ernie

   - Setiap percobaan login dan register akan tercatat pada log.txt dengan format : `YY/MM/DD hh:mm:ss MESSAGE`. Message pada log akan berbeda tergantung aksi yang dilakukan user.

     - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah `REGISTER: ERROR User already exists`
     - Ketika percobaan register berhasil, maka message pada log adalah `REGISTER: INFO User USERNAME registered successfully`
     - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah `LOGIN: ERROR Failed login attempt on user USERNAME`
     - Ketika user berhasil login, maka message pada log adalah `LOGIN: INFO User USERNAME logged in`

## 4. Syslog backup

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File `syslog` tersebut harus memiliki ketentuan :

   - Backup file log system dengan format jam:menit tanggal:bulan:tahun.
   - Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
     - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
     - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
     - Setelah huruf z akan kembali ke huruf a
   - Backup file syslog setiap 2 jam untuk dikumpulkan 😀.
   - Dan buatkan juga bash script untuk deskripsinya.

