#!/bin/bash

script_start_date=$(date +"%y/%m/%d %H:%M:%S")

user=$1
pass=$2

check_text="$1
$2"

USERS_DIR="${USERS_DIR:-/users}"
USERS_LIST="${USERS_LIST:-users.txt}"
USERS_PATH="${USERS_PATH:-$USERS_DIR/$USERS_LIST}"

if grep "$check_text" "$USERS_PATH"; then
  echo "$script_start_date [LOGIN] ERROR: Failed login attempt on user $user" | tee log.txt
  exit 1
fi

echo "$script_start_date [LOGIN] INFO: User $user logged in" | tee log.txt
