#!/bin/bash

script_start_date=$(date +"%y/%m/%d %H:%M:%S")

test_pass() {
  local rg_word="^((?!(${USER}|chicken|ernie)).)*$"
  local rg_pass="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})"
  if echo "$1" \
     | grep -ioP "$rg_word" \
     | grep -oP "$rg_pass" &> /dev/null
  then
    return 0;
  else
    return 1;
  fi
}

user=$1
pass=$2

if [ $# -lt 2 ]; then
  echo "Please specify username and password."
  exit 1
fi

if ! test_pass "$pass"; then
  echo '
Password requirements not met. Your password must:

- be at least 8 characters long,
- have at least one numeric digit,
- have at least lowercase letter,
- have at least uppercase letter,
- must not contain the word "'$USER'" (your UNIX username), "chicken", or "ernie"'
  exit 1
fi

USERS_DIR="${USERS_DIR:-/users}"
USERS_LIST="${USERS_LIST:-users.txt}"
USERS_PATH="${USERS_PATH:-$USERS_DIR/$USERS_LIST}"

[ -d $USERS_DIR ] || mkdir -p $USERS_DIR

err=$?

if [ $err -gt 0 ]; then exit $err; fi
 
if grep "$user" "$USERS_PATH"; then
  echo "$script_start_date [REGISTER] ERROR: User $user already exists" | tee log.txt
  exit 1
fi

echo "$user
$pass" >> "$USERS_PATH" && \
echo "$script_start_date [REGISTER] INFO: User $user registered successfully" | tee log.txt

