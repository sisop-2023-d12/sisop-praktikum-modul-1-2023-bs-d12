# Laporan Soal Shift Modul 1

Shell Scripting, Cron, dan AWK

Sistem Operasi Semester Genap 2023
Institut Teknologi Sepuluh Nopember  
Kelompok D12

| ID  | Nama                        | NRP            |
| --- | --------------------------- | -------------- |
| MH  | Muhammad Hidayat            | 05111940000131 |
| AM  | Afdhal Ma'ruf Lukman        | 05111940007001 |
| BS  | Muhammad Budhi Salmanjannah | 5025201084     |

## Soal 1

Dalam soal ini, kita diberi file csv yang digunakan untuk membantu bocchi melakukan pemilihan universitas dsb. Cara pengerjaannya adalah sebagai berikut

1. Bocchi ingin melihat Universitas top 10 di Jepang.

   Kendala: kendala yang dihadapi adalah saat menggunakan awk, saya sudah mencoba beberapa format untuk mengeluarkan hasil yang diinginkan. Namun, hasil tidak sesuai ekspektasi sehingga harus mencoba banyak format. Format yang sudah saya coba adalah 

   Menggunakan NR==10. Pada penggunaan NR, nilai n yang digunakan itu sangat spesifik. Contohnya jika ingin melihat Universitas top 10 di dunia, dengan menggunakan:

   Hasil yang didapatkan adalah top 9 universitas di dunia (karena yang di print hanya sampai line 10 atau universitas ke 9). Namun jika kita tambahkan /japan/ atau /JP/ disebelum print, hasil yang akan keluar adalah blank. Menurut saya hal ini terjadi karena NR hanya menunjukkan line yang spesifik saja. Universitas top jepang hanya ada di peringkat 50< sehingga nilai japan di line 1-10 tidak ada dan yang ditunjukkan blank saja.

   Solusi: Solusi yang digunakan adalah dengan menggunakan awk -F dan filter japan, kemudian lakukan piping ke head untuk memotong hasil yang awalnya ada puluhan menjadi 10.

   ![](./img/01-a.png)

2. Bocchi ingin melihat 5 Universitas dengan FSR terendah di jepang

   Kendala: Hasil penggunaan sort yang tidak sesuai dengan ekspektasi. Hasil yang paling pertama keluar adalah salah satu universitas dari tokyo dengan FSR 100.

   Solusi (?): menggunakan teknik yang sama dengan sebelumnya tapi sebelum piping ke head, kita gunakan sort untuk mengurutkan FSR dari yang paling bawah ke atas (ascending).

   ![](./img/01-b.png)

   ![](./img/01-c.png)

3. Output univ jepang dengan GER rank tertinggi

   Kendala: Hasil penggunaan sort tidak sesuai dengan ekspektasi. Hasil yang paling terakhir keluar adalah GER dengan rank terendah.

   Solusi: menggunakan teknik yang sama namun nilai $9 diganti menjadi $20. 
   ![](./img/01-d.png)

## Soal 2

b

## Soal 3

Regex yang digunakan:

```regex
(?=.*\d)         : Digits
(?=.*[a-z])      : Lowercase letters
(?=.*[A-Z])      : Uppercase letters
.{8,}        : Length

/((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})/
```

Regex kedua:

```regex
/^((?!(user|chicken|ernie)).)*$/i : Not match user, chicken, ernie
```

dimana `user` adalah nama user dari sistem tersebut, dan _flag_ `i` berarti _case-insensitive_.

Referensi:

- [How can I ensure a Bash string is alphanumeric, without an underscore?](https://stackoverflow.com/q/1229210)
- [Regular Expression for password validation](https://stackoverflow.com/q/2370015)
- [Regular expression to match a line that doesn't contain a word](http://stackoverflow.com/q/406230)
- [Regex: match everything but a specific pattern](https://stackoverflow.com/q/1687620)


## Soal 4

### Sandi Caesar

Sistem cipher/sandi yang dimaksud dalam soal adalah sandi Caesar, salah satu sistem persandian yang paling sederhana. Cara kerjanya adalah menggeser tabel alfabet sebesar nilai yang ditentukan, misal `n` sebagai kunci sandi. 

Misalkan terdapat sebuah teks berisi pesan berikut:

```
Refrain from attacking till they've defended
Or its DC
```

Pesan tersebut akan disandikan dengan kunci 5. Tabel alfabet/sandi menjadi seperti berikut:

```
ABCDEFGHIJKLMNOPRQSTUVWXYZ
FGHIJKLMNOPQRSTUVWXYZABCDE
```

Dengan demikian, pesan berubah menjadi:

```
Wjkwfns kwtr fyyfhpnsl ynqq ymjd'aj ijkjsiji
Tw nyx IH
```

Secara singkat, untuk tiap huruf alfabet yang ada di sebuah string, dilakukan fungsi seperti berikut:

```
foreach oldChar in oldText:
    newChar = (oldChar + n) mod 26
    newText.append(newChar)

return newText
```

Untuk bahasa pemrograman umum seperti C dan Python, cara ini bisa digunakan, namun lebih sulit untuk mengimplementasikan cara ini dalam bahasa shell scripting (bash). Untuk itu, saya gunakan cara lain, yaitu dengan membentuk tabel alfabet secara langsung (_hard-coded_), kemudian membentuk tabel enkripsi dengan manipulasi string.

Berikut cara membentuk tabel enkripsi:
1. Ambil tabel alfabet, dan buat dua salinan.
2. Untuk salinan pertama, buang `n` huruf pertama.
3. Untuk salinan kedua, buang `26-n` huruf pertama. Ini bertujuan untuk menyeimbangkan panjang tabel.
4. Gabungkan kedua salinan tersebut menjadi satu buah tabel enkripsi alfabet.

Dan berikut implementasi dalam Bash:

```bash
alpha_cap="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
c1=$(echo $alpha_cap | sed -r "s/^.{$shifts}//g")
c2=$(echo $alpha_cap | sed -r "s/.{$((26 - $shifts))}$//g")
cipher_cap=${c1}${c2}
```

Di sini saya menggunakan `sed` untuk manipulasi string menggunakan _extended regular expression (regex)_ (flag `-r`) agar bisa menggunakan sintaks di atas. Untuk penggunaan `sed` yang pertama, berikut penjelasan regex yang digunakan:

```
s/^.{$shifts}//g

s              ; lakukan pencocokan regex
/^.{$shifts}   ; cari n huruf pertama
/              ; hapus hasil pencarian tersebut dari hasil akhir
/g             ; lakukan pencarian global
```

Untuk `sed` kedua, terdapat sedikit perbedaan.

```
s/.{$((26 - $shifts))}$//g

s              
/.{$((26 - $shifts))}$  ; cari (26 - n) huruf terakhir
/                       ; hapus hasil pencarian tersebut dari hasil akhir
/g             
```

Langkah di atas dilakukan untuk huruf kapital dan huruf kecil dan digabungkan dalam satu tabel/string yang mencakup keduanya.

```bash
alpha=${alpha_low}${alpha_cap}
cipher=${cipher_low}${cipher_cap}
```

Terakhir, untuk mengenkripsi sebuah file, digunakan perintah `tr` (_transform_) untuk mengubah tiap huruf alfabet dalam file tersebut. 

```bash
cat "$infile" | tr "$alpha" "$cipher" > "$outfile"
```

Perintah `tr` menerima dua buah argumen yang nantinya bertindak sebagai tabel transformasi per huruf, tiap huruf di sebelah kiri akan digantikan oleh huruf di sebelah kanan pada posisi yang sama.

Untuk dekripsi, tabel sandi dan alfabet ditukar posisinya dalam perintah `tr`.

```bash
cat "$infile" | tr "$cipher" "$alpha" > "$outfile"
```

Implementasi lengkap dapat dilihat di file `log_encrypt.sh` dalam fungsi `encrypt` dan file `log_decrypt.sh` dalam fungsi `decrypt`.

### Backup `syslog`

File `syslog` pada distribusi Linux Ubuntu terdapat di `/var/log/syslog`. Karena file ini milik sistem dan hanya bisa diakses oleh root, untuk saat ini, script hanya bisa dijalankan oleh user biasa jika mereka bisa mengakses file `syslog` tanpa melakukan input password. Jika ternyata user diminta melakukan input password, script ini akan gagal.

Agar bisa diproses, file `syslog` disalin terlebih dahulu ke `~/.syslog` menggunakan `sudo cp`. Kemudian file tersebut diubah kepemilikannya menjadi milik user dengan `sudo chown`. Perintah `sudo` ditambahkan di sini agar bisa mengakses file `syslog. Jika salah satu dari kedua perintah ini gagal, script akan berhenti dengan error code `1`.

Format file backup syslog yang saya gunakan adalah `syslog_jam:menit_tanggal:bulan:tahun.txt` dan ditaruh di `~/syslog-backups` milik user yang menjalankan script ini. Untuk menentukan output secara otomatis, digunakan output dari perintah `date` dan dijadikan bagian dari nama file backup.

Referensi:

- [Caesar cipher](https://en.wikipedia.org/wiki/Caesar_cipher)
- [Caesar-Cipher-Bash](https://github.com/BornaAkbarshahi/Caesar-Cipher-Bash)
