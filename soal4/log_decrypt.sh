#!/bin/bash

## --- CIPHER PROCESS --- ##

alpha_low="abcdefghijklmnopqrstuvwxyz"
alpha_cap="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

decrypt() {
	local shifts=$1
	local infile=$2
	local outfile=$3
	# How this works, taking advantege of extended regex
	# 1. take the $alpha_{low,cap} and remove the first $shifts letters from it (example: 5)
	# 2. take the $alpha_{low,cap} and remove the last $(( 26 - $shifts )) letters from it (example: 21)
	# 3. put them together into the variable $cipher_{low,cap}
	cipher_low=$(echo $alpha_low | sed -r "s/^.{$shifts}//g")$(echo $alpha_low | sed -r "s/.{$((26 - $shifts))}$//g")
	cipher_cap=$(echo $alpha_cap | sed -r "s/^.{$shifts}//g")$(echo $alpha_cap | sed -r "s/.{$((26 - $shifts))}$//g")
	alpha=${alpha_low}${alpha_cap}
	cipher=${cipher_low}${cipher_cap}
	cat "$infile" | tr "$cipher" "$alpha" > "$outfile"
}

## --- PROGRAM --- ##

usage() {
	cat <<- END
	Usage: $(basename $0) IN_FILE [OUT_FILE]
	
	Note:
	  - If not specified, OUT_FILE will print to the standard output (stdout).
	  - If you want to encrypt/decrypt multiple files, you can call this program
	    for each of the files you would like to process.
	END
}

infile=$1
infile_base=$(basename $1)
outfile=${2:-/dev/stdout}

if [ $# -lt 1 ]; then
	usage
	exit 1
fi

rgx_get_hour="(?<=^syslog_)(\d{2})"

# TODO don't repeat checks
if ! echo $infile_base | grep -oP "$rgx_get_hour" &> /dev/null; then
	echo "Filename format mismatch."
	exit 1
fi

# workaround to force hour digits to decimal
shifts=$((10#$(echo $infile_base | grep -oP "$rgx_get_hour")))

decrypt $shifts $infile $outfile
