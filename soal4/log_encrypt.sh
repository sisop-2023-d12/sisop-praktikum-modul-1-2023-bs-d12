#!/bin/bash

## --- CIPHER PROCESS --- ##

alpha_low="abcdefghijklmnopqrstuvwxyz"
alpha_cap="ABCDEFGHIJKLMNOPQRSTUVWXYZ"

encrypt() {
	local shifts=$1
	local infile=$2
	local outfile=$3
	# How this works, taking advantege of extended regex
	# 1. take the $alpha_{low,cap} and remove the first $shifts letters from it (example: 5)
	# 2. take the $alpha_{low,cap} and remove the last $(( 26 - $shifts )) letters from it (example: 21)
	# 3. put them together into the variable $cipher_{low,cap}
	cipher_low=$(echo $alpha_low | sed -r "s/^.{$shifts}//g")$(echo $alpha_low | sed -r "s/.{$((26 - $shifts))}$//g")
	cipher_cap=$(echo $alpha_cap | sed -r "s/^.{$shifts}//g")$(echo $alpha_cap | sed -r "s/.{$((26 - $shifts))}$//g")
	alpha=${alpha_low}${alpha_cap}
	cipher=${cipher_low}${cipher_cap}
	cat "$infile" | tr "$alpha" "$cipher" > "$outfile"
}

## --- PROGRAM --- ##

syslog_file="${SYSLOG_FILE-/var/log/syslog}"
outdir="$HOME/syslog-backups"
format="syslog_$(date +%H:%M_%d:%m:%Y).txt"
outpath="$outdir/$format"

[ -d "$outdir" ] || mkdir -p "$outdir"

echo "Processing $syslog_file. Please provide your credentials if prompted."
if ! sudo -p "Please enter password for %p to process this file: " cp "$syslog_file" ./.syslog; then
	echo "Unable to copy $syslog_file. Please check if you have the permissions to do so."
  exit 1
fi
if ! sudo -p "Please enter password for %p to process this file: " chown $(id -u):$(id -g) ./.syslog; then
	echo "Unable to change permissions $syslog_file. Please check if you have the permissions to do so."
  exit 1
fi

shifts=$((10#$(date +%H)))

encrypt $shifts ./.syslog "$outpath"

rm ./.syslog

echo "$syslog_file backed up and encrypted to $outpath"

# CRONTAB
#
# Run every 2 hours at xx:00
# 0 */2 * * * /home/hand/syslog-bak/log_encrypt.sh
